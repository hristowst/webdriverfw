import java.util.Scanner;

public class Zadacha3 {

	public static void main(String[] args) {
		
		int c;
		
		Scanner scanner = new Scanner(System.in);
		//Before swapping
		System.out.println("First number: ");
		int a = scanner.nextInt(); 
		System.out.println("Your first number is: " + a);
		
		System.out.println("Second number: ");
		int b = scanner.nextInt();
		System.out.println("Your second number is: " + b);
		
		c = a;
		a = b;
		b = c;
		
		//After swapping
		System.out.println("Reversed number a: " + a);
		System.out.println("Reversed number b: " + b);
			
	}
 }
