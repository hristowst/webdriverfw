package homework7;

import java.util.ArrayList;
import java.util.Scanner;

public class task3 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number: ");
		int n = sc.nextInt();
		
		ArrayList<Integer> elements = new ArrayList<Integer>(10);
		elements.add(n);
		elements.add(n);

		for (int i = 2; i < 10; i++) {
			n = (i - 1) + (i - 2);

			elements.add(n);
		}
		
		System.out.println(elements.toString().replaceAll(",", " ").replace("]", " ").replace("[", " "));
	}

  }

