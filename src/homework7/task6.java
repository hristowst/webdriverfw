package homework7;

import java.util.ArrayList;
import java.util.Scanner;

public class task6 {

	public static void main(String[] args) {

		ArrayList<Integer> elements = new ArrayList<Integer>();
		ArrayList<Integer> elements2 = new ArrayList<Integer>();

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size on an array: ");
		int size = sc.nextInt();
		
		System.out.println("Enter values for the first array: ");
		for (int i = 0; i < size; i++) {
			int hasNext = sc.nextInt();
			elements.add(hasNext);
		}
		
		Scanner sc2 = new Scanner(System.in);
		System.out.print("Enter size on a second array: ");
		int size2 = sc.nextInt();
		
		System.out.println("Enter values for second array: ");
		for (int i = 0; i < size2; i++) {
			int hasNext2 = sc.nextInt();
			elements2.add(hasNext2);
		}
		
//		System.out.println(elements);
//		System.out.println(elements2);
		
		if(elements.equals(elements2)) {
			System.out.println("The arrays have the same size.");
		}else {
			System.out.println("The arrays are different.");
		}
	}

}
