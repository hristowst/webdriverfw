package homework7;

import java.util.ArrayList;
import java.util.Scanner;

public class task7 {

	public static void main(String[] args) {

		ArrayList<Integer> elements = new ArrayList<Integer>();

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size on an array: ");
		int size = sc.nextInt();
		
		System.out.println("Enter values for the array: ");
		for (int i = 0; i < size; i++) {
			int hasNext = sc.nextInt();
			elements.add(hasNext);
		}
		
		ArrayList<Integer> elements2 = new ArrayList<>();
		elements2.addAll(elements);


//		System.out.println("The array is: " + elements);
		System.out.println(elements2);
	}

}
