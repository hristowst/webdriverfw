import java.util.Scanner;

public class Zadacha7 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
	
		System.out.println("Hour: ");
	
		int hour = scanner.nextInt();
		System.out.println("Hour is: " + hour);
		
		System.out.println("Money: ");
		double money = scanner.nextDouble();
		System.out.println("Your amount of money is: " + money);
		
		System.out.println("Am I healthy?");
		boolean iAmHealthy = scanner.nextBoolean();
		
		if (!iAmHealthy) {
			System.out.println("I'm not going out.");
			if (money > 0) {
				System.out.println("I will buy medicines.");
			}else {
				System.out.println("I will stay home and drink tea.");
			}
		} else {
			if ((iAmHealthy) || (money < 10)){
				System.out.println("I will go to have coffee.");
	    }

	}
		
  }
	
}
