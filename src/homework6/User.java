package homework6;

public interface User {
	void logIn();
	void logOut();
	String getUsername();
	String getRegistrationDate();
	Boolean isAdmin();
}
