package homework6;

import java.util.ArrayList;
import java.util.List;

public class DataBase {
	
	List<User> users;
	
	//konstruktor, koito priema kolko da e golqm lista
	public DataBase(int size) {
		users = new ArrayList<User>(size);
	}

	//getter, koito dava vyzmojnost da se vzemat vs potrebiteli
	public List<User> getUsers() {
		return users;
	}
	
	public void addUsers(User newUser) {
		users.add(newUser);
	}
}
