package homework6;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public abstract class AbstractUser implements User {

	//tuk se iznasqt obshtite neshta mejdu userite
	String userName;
	String password;
	Date registrationDate;
	Boolean isAdmin;
	
	public Boolean isAdmin() {
		return isAdmin;
	}
	
	public void logIn() {
		System.out.println("User " +userName+ " is logged in.");
	}
	
	public void logOut() {
		System.out.println("User " +userName+ " logged out.");
	}

	public String getUsername() {
		System.out.println("Username is " + userName);
		return this.userName;
	}
	
	public String getRegistrationDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		System.out.println("User is registered on " + dateFormat.format(this.registrationDate));
		return dateFormat.format(this.registrationDate);
	}

	public void createUser(String userName, String password, boolean isAdmin) {
		// TODO Auto-generated method stub
		
	}

	public void deleteUser(String userName) {
		// TODO Auto-generated method stub
		
	}

	public List<User> lookAtAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}
}


