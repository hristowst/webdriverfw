package homework6;

public class Demo {

	public static void main(String[] args) {
		
		AdminUser admin = new AdminUserlmpl("admin1", "admin1");
//		admin.logIn();
//		admin.getUsername();
//		admin.getRegistrationDate();
		admin.createUser("user1", "user1", false);
		admin.createUser("user2", "user2", false);
		admin.createUser("user3", "user5", false);
//		admin.createUser("user6", "user6", false);
//		admin.createUser("user7", "user7", false);
//		admin.createUser("user8", "user8", false);
//		admin.createUser("user9", "user9", false);
//		admin.createUser("user2", "user2", false);
//		admin.createUser("admin3", "admin3", true);
//		admin.createUser("admin10", "admin10", true);
//
//		admin.createUser("admin11", "admin11", true);

		
		for(User usr : admin.lookAtAllUsers()) {
			System.out.println("I am " + usr.getUsername());
			//System.out.println("I am Admin user: " + usr.isAdmin());
		}
		
		admin.deleteUser("user2");
		for(User usr : admin.lookAtAllUsers()) {
			System.out.println("I am " + usr.getUsername());
			//System.out.println("I am Admin user: " + usr.isAdmin());
		}
	}

}
