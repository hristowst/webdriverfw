package homework6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ListEX {

	public static void main(String[] args) {

		//rabotqt otzad sys  starite masivi , no ne gi vijdame i e mn po-priqtno 
		List<String> names = new ArrayList();
		List<String> otherNames = new ArrayList();

		
		//dokato obhojdam list shte iskam da izchezne neshto ot masiva da go iztriq, sega iskam da iztriq "Stefan"
		names.add("Ivan");
		names.add("Maria");
		names.add("Nika");
		names.add("Ivan");
		names.add("Stefan");
		names.add("Ivan");
		names.add("Maria");
		names.add("Nika");
		names.add("Ivan");
		names.add("Maria");

		//doakto obhojdash kolekciq nemoje da q modificirash osven ako ne polzvash iteratora za da mojete da q modificirate
//		for (String curNames : names) {
//			if(curNames.equals("Stefan")) {
//				names.remove("Stefan");
//			}
//		}
//		
	  
		//iterator za da ne dava greshka - specialen obekt koito poznava po-dobre kolekciqta i premahva element ot neq kato iskash da mahash ti trqbva iterator
		Iterator<String> iterator = names.iterator();
		
		while(iterator.hasNext()) {
			String curNames = iterator.next();
			if(curNames.equals("Maria")) {
				iterator.remove();
			}
		}
		
		System.out.println(names.toString());
		
		
		//pri list se printirat duplicated names, ne kato pri set - tam nqma da se printirat duplicated names 
//		for (String curName : names) {
//			System.out.println(curName);
//		}
//		
//		System.out.println(names.get(3));
	}

}
