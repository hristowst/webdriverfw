package homework6;

import java.util.List;

public interface AdminUser extends User{

	void deleteUser(String userName);
	void createUser(String userName, String password, boolean isAdmin);
	List<User> lookAtAllUsers();
}
