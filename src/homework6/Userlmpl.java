package homework6;

import java.util.Date;

public class Userlmpl extends AbstractUser {

	public Userlmpl(String username, String password) {
		this.userName = username;
		this.password = password;
		this.isAdmin = false;
		this.registrationDate = new Date();
	}

}
