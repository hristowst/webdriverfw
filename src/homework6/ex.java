package homework6;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

public class ex {

	public static void main(String[] args) {

		String value = "22.54";
		double parseDouble = Double.parseDouble(value);
		System.out.println(parseDouble);
		
		//SET EXAMPLES
		//Set<String> names = new HashSet();
		
		
		//likedhashset izprintva po red na vkarvaneto na imenata :) 
		Set<String> names = new LinkedHashSet();
		Set<String> otherNames = new HashSet();

		otherNames.add("Ivan2");
		otherNames.add("Maria2");
		otherNames.add("Nika2");
		otherNames.add("Ivan2");
		otherNames.add("Maria2");

		
		names.add("Ivan");
		names.add("Maria");
		names.add("Nika");
		names.add("Ivan");
		names.add("Maria");
		
		names.addAll(otherNames);

		//hashset obhojda ne pored na vlizane , a kakto toi si preceni , nai-burzo raboteshtoto , po podrazbirane raboti s
		//tova da ne pozvolqva duplicated da vliza vyv vuprosnata kolekciq i za tova povtarqshtite se imena sa izprinteni samo po eidn put
//		for (String currentName : names) {
//			System.out.println(currentName);
//		}
//		
		for (String currentName : names) {
			System.out.println(currentName);
		}
	}

}
