package homework6;

import java.util.Date;
import java.util.List;

public class AdminUserlmpl extends AbstractUser implements AdminUser{

	public DataBase db;
	
	public AdminUserlmpl(String username, String password) {
		this.userName = username;
		this.password = password;
		this.isAdmin = true;
		this.registrationDate = new Date();
		this.db = new DataBase(10);
	}
	
	@Override
	public void deleteUser(String userName) {
		if(db.getUsers().size() == 0) {
			System.out.println("DB is empty.");
			return;
		}
		for (User user: db.getUsers()) {
			if(user.getUsername().equals(userName)) {
				db.getUsers().remove(user);
			}
		}
		System.out.println("User " + userName +  " is deleted.");
	}

	@Override
	public void createUser(String userName, String password, boolean isAdmin) {
		
		for (User user : db.getUsers() ) {
			if(user.getUsername().equals(userName)) {
				System.out.println("User already exist.");
				return;
			}else if(db.getUsers().size() >= 10) {
				System.out.println("There is no more space.");
				return;
			}
		}
		
		if(isAdmin) {
			db.addUsers(new AdminUserlmpl(userName, password));
		}else {
			db.addUsers(new Userlmpl(userName, password));
		}
		System.out.println("User " + userName +  " is created and is admin: " + isAdmin);
	    
	}

	@Override
	public List<User> lookAtAllUsers() {
		return db.getUsers();
	}
	
}
