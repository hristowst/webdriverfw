
import java.util.Scanner;

public class Zadacha1 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter number A: ");
		//Waiting to input number from keyboard
		double a = scanner.nextDouble();
		System.out.println("Your number A is equal to: " + a);
		
		
		System.out.println("Enter number B: ");
		//Waiting to input number from keyboard
		double b = scanner.nextDouble();
		System.out.println("Your number B is equal to: " + b);
		
		System.out.println("Enter number C: ");
		double c = scanner.nextDouble();

		//First option  
        if(c > a && c < b ) {
	    System.out.println("Number " + c + " is between " + a + " and " + b );
	    }else {
	  	System.out.println("Number " + c + " is not between " + a + " and " + b );
        }
     
		//Second option (btw numbers in both orders - descending and ascending)
	/*	if(c > Math.min(a, b) && c < Math.max(a, b)) {
     *	System.out.println("Number " + c + " is between " + a + " and " + b );
	 *	}else {
	 *		System.out.println("Number " + c + " is not between " + a + " and " + b );
	 *	}
	*/	
	}

}
