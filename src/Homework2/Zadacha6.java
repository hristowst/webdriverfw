package Homework2;

import java.util.Scanner;

public class Zadacha6 {

	//sbora na vs chisla mejdu 1 i vuvedenoto chislo
	public static void main(String[] args) {
		
		System.out.println("Enter n: ");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		
		int sum = 0;
		for(int i = 1; i <= n; i++) {
			sum += i;
		}
		System.out.println("The result is: " + sum);

	}

}
