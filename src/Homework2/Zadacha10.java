package Homework2;

import java.util.Scanner;

public class Zadacha10 {

	public static void main(String[] args) {
		// If it is a prime number
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter n: ");
		int num = sc.nextInt();
		
		
		boolean prime = false;
        for(int i = 2; i <= num/2; ++i){
            // condition for non prime number
            if(num % i == 0){
                prime = true;
                break;
            }
        }
        
        if (!prime){
            System.out.println(num + " is a prime number.");
        }else {
            System.out.println(num + " is not a prime number.");
        }
    }
	
}


