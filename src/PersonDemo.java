
public class PersonDemo {

	//izpolzvame tuk kakvoto sme deklarirali na klasa s kolkoto iskame razlicni obekti
	public static void main(String[] args) {
		
		Person ivan = new Person();
		Person maria =  new Person();
		Car audi = new Car(null, false, null);
		audi.color = "Yellow";
		audi.maxSpeed = 100;
		
		//crv , true .. -> argumenti
		Car honda = new Car("crv", true, "white");
		System.out.println(honda.color);
		
		Car lada = new Car(null, false, null);
		lada.color = "Green";
		lada.doors = 2;
		
		ivan.name = "Ivan Ivanov";
		ivan.age = 35;
		ivan.weight = 105.30;
		
		maria.name = "Maria Paskova";
//		maria.age = 15;
		maria.weight = 46;
		
		System.out.println(maria.name);
		System.out.println(maria.age);
		
		System.out.println(ivan.name);
		
		lada.owner = ivan;
		System.out.println(lada.owner.name);
		
		ivan.friend = maria;
		System.out.println(ivan.friend.name);
		
		ivan.eat();
		maria.eat();

	}

}

