import java.util.Scanner;

public class Zadacha6 {

	public static void main(String[] args) {
		
		int a4;

		Scanner scanner = new Scanner(System.in);
		//Before swapping
		System.out.println("First number: ");
		int a1 = scanner.nextInt(); 
		System.out.println("a1: " + a1);
		
		System.out.println("Second number: ");
		int a2 = scanner.nextInt();
		System.out.println("a2: " + a2);
		
		System.out.println("Third number: ");
		int a3 = scanner.nextInt();
		System.out.println("a3: " + a3);
		
		a4 = a1;
		a1 = a2;
		a2 = a3;
		a3 = a4;
		
		//After swapping
		System.out.println("Reversed number a1: " + a1);
		System.out.println("Reversed number a2: " + a2);
		System.out.println("Reversed number a3: " + a3);

	}

}
