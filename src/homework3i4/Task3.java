package homework3i4;

import java.util.Arrays;
import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number: ");
		int n = sc.nextInt();
		
		int[] arr = new int [10];
		arr[0] = n;
		arr[1] = n;
		
		for (int i = 2; i < arr.length; i++) {
			arr[i] = arr[i - 1] + arr[i - 2];
		}
		//printing the array without commas and brackets
		System.out.println(Arrays.toString(arr).replaceAll(",", " ").replace("]", " ").replace("[", " "));
	
   }
}