package homework3i4;

public class Computer {

	int year;
	double price;
	boolean isNotebook;
	double hardDiskMemory;
	double freeMemory;
	String operationSystem;
	
	void changeOperationSystem(String newOperationSystem) {
		operationSystem = newOperationSystem;
		System.out.println("The new operation system is: " + newOperationSystem);
	}
	
	void useMemory(double memory) {
		freeMemory = freeMemory - memory;
		if(memory > freeMemory) {
			System.out.println("Not enough free memory!");
		}
	}
	
	Computer() {
		this.isNotebook = false;
		this.operationSystem = "Win XP";
	}
	
	Computer(int year, double price, double hardDiskMemory, double freeMemory) {
		//izvikva konstruktora po podrazbirane
		this();
		//inicializira ostanalite poleta sys stoinostite podadeni kato argumenti
		this.year = year;
		this.price = price;
		this.hardDiskMemory = hardDiskMemory;
		this.freeMemory = freeMemory;
	}

	 Computer(int year, double price, boolean isNotebook, double hardDiskMemory, double freeMemory,String operationSystem) {
		this.year = year;
		this.price = price;
		this.isNotebook = isNotebook;
		this.hardDiskMemory = hardDiskMemory;
		this.freeMemory = freeMemory;
		this.operationSystem = operationSystem;
	}
	
	int comparePrice(Computer c) {
		if(this.price > c.price) {
			return -1;
		}else if(this.price == c.price) {
			return 0;
		}else {
			return 1;
		}
	}
	
}
