package homework3i4;

public class StudentGroup {
	String groupSubject;
	Student[] students;
	int freePlaces;
	int studentsCount;
	private double grade;
	private String name;

	StudentGroup() {
		this.freePlaces = 5;
		this.studentsCount = 0;
		students = new Student[5];
	}

	StudentGroup(String subject) {
		this();
		this.groupSubject = subject;
	}
	
	void addStudent(Student student) {
		if((groupSubject == student.subject) && ((freePlaces - 1) >= 0)) {
			students[studentsCount] = student;
			freePlaces--;
			studentsCount++;
		}
	}
	
	public Student[] getStudents(){
		return this.students;
	}

	public int getStudentCount(){
		return this.studentsCount;
	}
	
	void emptyGroup() {
		students = new Student[5];
		this.freePlaces = 5;
	}
	
	String findBestStudent() {
		Student bestStudent = students[0];
		
		for (int i = 1; i < students.length - freePlaces; i++) {
			if(students[i].grade > bestStudent.grade) {
				bestStudent = students[i];
			}
		}
		return bestStudent.name;
	}
	
	void printStudentsInGroup() {
		for (int i = 0; i < students.length - freePlaces; i++) {
			System.out.println("Name: " + students[i].name + " age: " + students[i].age);
		}
	}
	
//	double getGrades() {
//		return this.grade;
//	}
//	
//	Student[] theBestStudent() {
//	    return this.students;
//	}
//	
//	String printStudentsInGroup() {
//		System.out.println("Name: " + this.name);
//		
//		return groupSubject;
//	}
}
