package homework3i4;

public class Student {

	String name;
	String subject;
	double grade;
	int yearInCollege;
	int age;
	boolean isDegree;
	double money;
	
	Student() {
		this.grade = 4.0;
		this.yearInCollege = 1;
		this.isDegree = false;
		this.money = 0;
	}

    Student(String name, String subject, double grade, int yearInCollege, int age, boolean isDegree, double money) {
    	this();
		this.name = name;
		this.subject = subject;
		this.grade = grade;
		this.yearInCollege = yearInCollege;
		this.age = age;
		this.isDegree = isDegree;
		this.money = money;
	}
	
    void upYear() {
    	if(isDegree == false) {
    		yearInCollege++;
    		if (yearInCollege == 4) {
    			isDegree = true;
    		}
    	}
    	else {
    		System.out.println("Student has a degree in college.");
    	}
    }
    
    double receiveScholarship(double minGrade, double amount) {
    	if((grade >= minGrade) && (age < 30)) {
    		money += amount;
    	}
		return money;
    }

    String getName(){
		return this.name;
	}
    
}
