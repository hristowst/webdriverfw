package homework3i4;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] numbers) {

//		Scanner sc = new Scanner(System.in);
//		System.out.println("Enter size of an array: ");
//		int n = sc.nextInt();
//		
//	    int[] arr = new int[n];
//		int maxValue = 2147483647;
//		int minValue = 0;
//		minValue = maxValue;
//
//
//		for (int i = 0 ; i < arr.length; i++) {
//			System.out.println("Enter value: ");		
//			arr[i] = sc.nextInt();
//			
//			if (arr[i] % 3 == 0 && arr[i] < minValue) {
//				minValue = arr[i];
//		    }
//		}
//		
//		System.out.println("Nai-malkoto chislo kratno na 3 e: " + minValue);
//	}
		Scanner sc = new Scanner(System.in);
		//create the array
		System.out.println("Enter size for the array:");
		int size = sc.nextInt();
		int[] array = new int[size];
		
		//read the array from the console
		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter value for the index " + i);
			array[i] = sc.nextInt();
		}
		//find the minimum number multiple by 3
		boolean hasNumberMultipleBy3 = false;
		int min = array[0];
		
		for (int i = 1; i < array.length; i++) {
			if(array[i] % 3 == 0 && (min % 3 != 0 || array[i] < min)) {
				hasNumberMultipleBy3 = true;
				min = array[i];
			}
		}
		
		if(hasNumberMultipleBy3 == false && min % 3 != 0) {
			System.out.println("No number multiple by 3 in the array");
		} else {
			System.out.println("The minimum number multiple by 3 is " + min);
		}
	}

}