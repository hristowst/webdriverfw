package homework3i4;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {

//		Scanner sc = new Scanner(System.in);
//		System.out.println("Enter the size of an array % 2 == 0 : ");
//		int n = sc.nextInt();
//		if (n % 2 ==0 ) {
//			int[] arr = new int[n];
//			
//			//da se podkani da vuvede polovinata ot elementite na masiva
//			for (int i = 0 ; i < arr.length / 2; i++) {
//				System.out.println("Enter value: ");		
//				arr[i] = sc.nextInt();
//			}
//			System.out.println(Arrays.toString(arr));
//		}else {
//			System.out.println("You cannot continue :) ");
//		}
		
		Scanner sc = new Scanner(System.in);
		//create the array
		int size = 1;
		while(size % 2 != 0) {
			System.out.println("Enter even size for the array:");
			size = sc.nextInt();
		}
		
		int[] array = new int[size];
		for (int i = 0; i < array.length / 2; i++) {
			System.out.println("Enter value for the index " + i);
			array[i] = sc.nextInt();
			array[array.length / 2 + i] = array[i];
		}
		
		//print the array
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		
	}

}
