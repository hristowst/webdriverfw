package homework3i4;

public class ComputerDemo {

	public static void main(String[] args) {
		
		//dva obekta ot tip Computer
		Computer laptop = new Computer();
		Computer pc2 = new Computer();

		//Suzdavane na stoinosti na vseki ot komputrite
		laptop.year = 2000;
		laptop.price = 900.60;
		laptop.hardDiskMemory = 100;
		laptop.freeMemory = 30.50;
		laptop.operationSystem = ("Windows 8");

		pc2.year = 2006;
		pc2.price = 1800.60;
		pc2.hardDiskMemory = 250.30;
		pc2.freeMemory = 198;
		pc2.operationSystem = ("Windows 8");

		laptop.changeOperationSystem("Windows 10");
		pc2.useMemory(100);
		
		System.out.println("Laptop year: " + laptop.year);
		System.out.println("Laptop price: " + laptop.price);
		System.out.println("Laptop hard memory: " + laptop.hardDiskMemory);
		System.out.println("Laptop free memory: " + laptop.freeMemory);
		System.out.println("Laptop OS: " + laptop.operationSystem);

		System.out.println("PC2 year: " + pc2.year);
		System.out.println("PC2 price: " + pc2.price);
		System.out.println("PC2 hard memory: " + pc2.hardDiskMemory);
		System.out.println("PC2 free memory: " + pc2.freeMemory);
		System.out.println("PC2 OS: " + pc2.operationSystem);
		
		//obekti suzdadeni posrdstvom nalichnite konstruktori
		Computer pc3 = new Computer(2001, 1200, 200, 100);
		Computer pc4 = new Computer(2017, 2000, false, 600, 500, "Windows 10");
		
		//sravnqvane na cenite na nqkoi ot komputrite
		int comparison =pc2.comparePrice(laptop);
		System.out.println(comparison);
		System.out.println("PC2 price: " + pc2.price + " is more expensive than " + "laptop's price: " + laptop.price);
		
		int comparison1 = pc3.comparePrice(pc4);
		System.out.println(comparison1);
		System.out.println("PC3 price: " + pc3.price + " is less expensive than " + "PC4's price: " + pc4.price);
		
	}
}
