package homework3i4;

public class College {

	public static void main(String[] args) {
		//studenti
		Student st1 = new Student("Gosho2", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st2 = new Student("Gosho", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st3 = new Student("Ina", "English", 3.0, 3, 24, false, 0);
		Student st4 = new Student("Maria", "Engineering", 6.0, 4, 25, true, 2000);
		Student st5 = new Student("Ivan", "Physics", 5.3, 3, 28, false, 200);
		Student st6 = new Student("Gosho3", "English", 4.5 , 2 , 22 , false, 650);
		Student st7 = new Student("Gosho4", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st8 = new Student("Gosho5", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st9 = new Student("Gosho6", "Maths", 4.5 , 2 , 22 , false, 650);
		
		//dobavqne na stipendii nq nqkoi ot studentite
		double result = st1.receiveScholarship(3.5, 100);
		System.out.println(result);
		
		double result1 = st5.receiveScholarship(3.5, 100);
		System.out.println(result1);
		
		//prehvurlq nqkoi v po-gorna godina
	    st2.upYear();
	    System.out.println("ST2 is now: " + st2.yearInCollege + "year in college.");
	    
	    st4.upYear();
	    System.out.println(st4.yearInCollege);

	    //grupi ot studenti
		StudentGroup gr1 = new StudentGroup("Maths");
		StudentGroup gr2 = new StudentGroup("English");
		StudentGroup gr3 = new StudentGroup("Physics");

		//dobavqne na studenti v grupite
		gr1.addStudent(st1);
		gr1.addStudent(st2);
		gr1.addStudent(st7);
		gr1.addStudent(st8);
		gr1.addStudent(st9);
		
		gr2.addStudent(st3);
		gr2.addStudent(st6);


		Student[] stArr = gr1.getStudents();
		for(int i = 0; i < stArr.length; i++) {
			System.out.println(String.format("Student %s is group 1.", stArr[i].getName()));
		}
		
		String bestStudent = gr1.findBestStudent();
		System.out.println("The best student is: " + bestStudent);
		gr1.printStudentsInGroup();
	
	}

}
