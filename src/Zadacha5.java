import java.util.Scanner;

public class Zadacha5 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("First number: ");
		int a = scanner.nextInt(); 
		System.out.println("Your first number is: " + a);
		
		System.out.println("Second number: ");
		int b = scanner.nextInt();
		System.out.println("Your second number is: " + b);
		
		System.out.println("Third number: ");
		int c = scanner.nextInt();
		System.out.println("Your third number is: " + c);
		
		int  min, max, med;
		
        if(a > b){
             if(a > c){
              max = a;
              if(b > c){
               med = b;
               min = c;
              }else{
               med = c;
               min = b;
              }
             }else{
              med = a;
              if(b > c){
               max = b;
               min = c;
              }else{
               max = c;
               min = b;
              }
             }
            }else{
             if(b > c){
              max = b;
              if(a > c){
               med = a;
               min = c;
              }else{
               med = c;
               min = a;
              }
             }else{
              med = b;
              max = c;
              min = a;
             }
            }

        //Display results
        System.out.println("Descending order: " + max +"," + med + "," + min);
	}
}
