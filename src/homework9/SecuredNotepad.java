package homework9;

import java.util.Scanner;

public class SecuredNotepad extends SimpleNotepad {
	private String password;

	public SecuredNotepad(String password) {
		this.password = password;
	}

	private boolean checkPassword() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Dai parola be: ");
		String enteredPassword = scan.nextLine();
		
		if(enteredPassword.equals(password)) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public void createPage(String title, String text) {
		if(checkPassword()) {
			super.createPage(title, text);
		} else {
			System.out.println("Create Page will NOT happen");
		}
	}

	@Override
	public void replaceText(int pageNumber, String text) throws NoSuchPageNumber {
		if(checkPassword()) {
			super.replaceText(pageNumber, text);
		} else {
			System.out.println("Replace Text will NOT happen");
		}
		
	}

	@Override
	public void deleteText(int pageNumber) throws NoSuchPageNumber {
		if(checkPassword()) {
			super.deleteText(pageNumber);
		} else {
			System.out.println("Delete TExt will NOT happen");
		}
	}

	@Override
	public void previewAllPages() {
		if(checkPassword()) {
			super.previewAllPages();
		} else {
			System.out.println("PrintAllPages will NOT happen");
		}
	}

}
