package homework9;

import java.util.ArrayList;
import java.util.List;

public class SimpleNotepad  implements Notepad, IElectronicDevice {

	Boolean isStarted;
	private List<Page> pages;
	
	public SimpleNotepad() {
		this.pages = new ArrayList<Page>();
	}

	@Override
	public void createPage(String title, String text) {
		Page page = new Page(title, text);
		this.pages.add(page);
		page.setPageNumber(this.pages.size());
	}

	@Override
	public void replaceText(int pageNumber, String text) throws NoSuchPageNumber {
		Page foundPage = findPage(pageNumber);
		foundPage.deleteText();
		foundPage.addText(text);
	}

	private Page findPage(int pageNumber) throws NoSuchPageNumber {
		for (Page curPage : pages) {
			if(curPage.getPageNumber() == pageNumber) {
				return curPage;
			}
		}
		
		throw new NoSuchPageNumber("Page with number " + pageNumber + " does not exist.");
	}
	
	@Override
	public void deleteText(int pageNumber) throws NoSuchPageNumber {
		Page foundPage = findPage(pageNumber);
		foundPage.deleteText();
	}

	@Override
	public void previewAllPages() {
		for (Page curPage : pages) {
			String returnedTextAndTitle = curPage.previewPage();
			System.out.println(returnedTextAndTitle);
		}
	}

	@Override
	public void stop() {
		
	}



	@Override
	public void start() {
		
	}

	@Override
	public Boolean isStarted() {
		return isStarted;
	}


}
