package homework9;

public class ElectronicSecuredNotepad extends SecuredNotepad implements IElectronicDevice{

	Boolean isStarted;
	public ElectronicSecuredNotepad(String password) {
		super(password);
	}

	@Override
	public void start() {
		System.out.println("The device is starting...");		
	}

	@Override
	public void stop() {
		System.out.println("The device is stopping...");
		
	}

	@Override
	public Boolean isStarted() {
		if(isStarted) {
			System.out.println("It is started.");
		}
		return isStarted;
	}

}
