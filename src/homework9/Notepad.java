package homework9;

public interface Notepad extends IElectronicDevice {

	/**
	 * Creates a new page and adds the title and texts provided
	 * as parameters to it
	 * 
	 * @param title the title that you would expect the page to has
	 * @param text the text that will be added to the page
	 * 
	 * @author Strahinski
	 */
	void createPage(String title, String text);
	
	/**
	 * Replaces the text of a specific page based on a
	 * provided page number
	 * @param pageNumber the number of the page of which you want to have the text replaced
	 * @param text the new text that will be replaced on the page
	 * @throws NoSuchPageNumber 
	 */
	void replaceText(int pageNumber, String text) throws NoSuchPageNumber;
	
	/**
	 * Deletes the text of a specific page in the notepad
	 * 
	 * @param pageNumber the number of the page you want to have the text deleted
	 * @throws NoSuchPageNumber 
	 */
	void deleteText(int pageNumber) throws NoSuchPageNumber;

	void previewAllPages();

	
	/**
	 * Prints all the pages with their text 
	 * and their title on the screen
	 */

}
