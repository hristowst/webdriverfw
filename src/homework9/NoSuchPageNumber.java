package homework9;

public class NoSuchPageNumber extends Exception {
	
	private static final long serialVersionUID = 1L;

	public NoSuchPageNumber() {
		super();
	}

	public NoSuchPageNumber(String message, Throwable cause) {
		super(message, cause);
	}

	public NoSuchPageNumber(String message) {
		super(message);
	}

	public NoSuchPageNumber(Throwable cause) {
		super(cause);
	}

}
