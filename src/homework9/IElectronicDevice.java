package homework9;

public interface IElectronicDevice  {
	
	void start();
	void stop();
	Boolean isStarted();
	void createPage(String title, String text);

}
