import java.util.Scanner;

public class Zadacha4 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("First number: ");
		int a = scanner.nextInt(); 
		System.out.println("Your first number is: " + a);
		
		System.out.println("Second number: ");
		int b = scanner.nextInt();
		System.out.println("Your second number is: " + b);
		
		if(a < b) {
			System.out.println("Increasing order: " + a + " , " + b);
		}else {
			System.out.println("Increasing order: " + b + " , " + a);
		}

	}

}
