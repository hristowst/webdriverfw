package homework5;

public class Employee extends Person {

	//constant variable - za vsichki instancii na tozi klass , neinata stoinost shte e edna i sushta
	private final static double OVERTIME_SALARY = 0.0;
	
	private double daySalary;

	public Employee(String name, int age, boolean isMan, double daySalary) {
		super(name, age, isMan);
		this.daySalary = daySalary;
	}
	
	public double calculateOvertime(double hours) {
		if(super.getAge() < 18) {
			System.out.println("No 18 years");
			return OVERTIME_SALARY;
		}else {
			return getOvertimeMoney(hours);
		}
	}

	public double getOvertimeMoney(double hours) {
		double moneyPerHour = daySalary / 8;
        return hours * (moneyPerHour * 1.5);
	}

	@Override
	public void showEmployeeInfo() {
		System.out.println(String.format("Name: %s%n" + "Age: %d" , super.getName(), super.getAge()));
    	if(super.isMan()) {
    		System.out.println("Sex: male");
    	}else { 
    		System.out.println("Sex: female");
    	}
		System.out.println(String.format("Daily salary: %f%n", this.daySalary));
	}
	
}
