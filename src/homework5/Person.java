package homework5;

public class Person {
	
	private String name;
	private int age;
	private boolean isMan;
	
	
    Person(String name, int age, boolean isMan) {
		this.name = name;
		this.age = age;
		this.isMan = isMan;
	}
    
    public void showPersonInfo() {
    	System.out.println(String.format("Name: %s%n" + "Age: %d" , this.name, this.age));
    	if(isMan) {
    		System.out.println("Sex: male");
    	}else { 
    		System.out.println("Sex: female");
    	}
    }
    
    public void showStudentInfo() {}

    public void showEmployeeInfo() {}

    //generating getters and setters 
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge() {
		this.age = age;
	}

	public boolean isMan() {
		return isMan;
	}

	public void setMan(boolean isMan) {
		this.isMan = isMan;
	}



}
