package homework5;

public class Student extends Person {

	private double score;

	Student(String name, int age, boolean isMan, double score) {
		super(name, age, isMan);
		this.score = score;
	}

	@Override
	public void showStudentInfo() {
		System.out.println(String.format("Student's name: %s%n" + "Student's ge: %d" , super.getName(), super.getAge()));
		if(super.isMan()) {
    		System.out.println("Sex: male");
    	}else { 
    		System.out.println("Sex: female");
    	}

		if (score < 2 || score > 6) {
            System.out.println("The score must be btw 2 and 6.");
        }else {
           System.out.println(super.getName() + "'s score is equal to: " + this.score);
        }
	}
	
}
