package homework5;

public class PersonDemo {

	public static void main(String[] args) {

		Person[] persons = new Person[10];
		
		Person p1 = new Person("Gosho", 25, true);
		Person p2 = new Person("Maria", 26, false);
		
		Student st1 = new Student("Niko", 18, true, 3.5);
		Student st2 = new Student("Ina", 20, false, 3.5);

		Employee em1 = new Employee("Penka", 26, false , 30);
		Employee em2 = new Employee("Petyr", 27, true , 40);

		persons[0] = new Person("Gosho", 25, true);
		persons[1] = new Person("Maria", 26, false);
	    persons[2] = new Student("Niko", 18, true, 3.5);
	    persons[3] = new Student("Ina", 20, false, 3.5);
	    persons[4] = new Employee("Penka", 26, false , 30);
	    persons[5] = new Employee("Petyr", 27, true , 40);

	    //casting
	    //using instanceof before casting to be safe
	    for (int i = 0; i < persons.length; i++) {
	    	if(persons[i] instanceof Person) {
                Person personReference = (Person)persons[i];
                personReference.showPersonInfo();
                } else if(persons[i] instanceof Student) {
                Person studentReference = (Person)persons[i];
                studentReference.showStudentInfo();
                } else if(persons[i] instanceof Employee) {
                        Person employeeReference = (Person)persons[i];
                        employeeReference.showEmployeeInfo();
                }
			}
	    
	    for (int i = 0; i < persons.length; i++) {
			if(persons[i] instanceof Employee) {
				Employee employeeReference = (Employee)persons[i];
                System.out.println("Owed amount of overtime: " + employeeReference.getOvertimeMoney(2) + " leva."); 
			}
		}
		
	    
	    
	}
}
