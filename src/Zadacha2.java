import java.util.Scanner;

public class Zadacha2 {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		//Whole numbers - example
		System.out.println("First number: ");
		int number1 = scanner.nextInt();
		System.out.println("Your first number is: " + number1);
		
		System.out.println("Second number: ");
		int number2 = scanner.nextInt();
		System.out.println("Your second number is: " + number2);
				
		int resultSum = number1 + number2;
		System.out.println("Sum of numbers is equal to: " + resultSum);
		
		int resultDifference = number1 - number2;
		System.out.println("Difference between numbers is equal to: " + resultDifference);
		
		int resultMultiply = number1 * number2;
		System.out.println("Multiplication of numbers is equal to: " + resultMultiply);
		
		int resultRemainder = number1 % number2;
		System.out.println("Remainder of division of numbers is equal to: " + resultRemainder);
		
		int resultDivide = number1 / number2;
		System.out.println("Division of numbers is equal to: " + resultDivide);
		
		//Floating-point numbers - example
		System.out.println("Third number: ");
		double number3 = scanner.nextDouble();
		System.out.println("Your third number is: " + number3);
		
		System.out.println("Fourth number: ");
		double number4 = scanner.nextDouble();
		System.out.println("Your fourth number is: " + number4);
				
		double resultSumDoubleNumber = number3 + number4;
		System.out.println("Sum of numbers is equal to: " + resultSumDoubleNumber);
		
		double resultDifferDoubleNumber = number3 - number4;
		System.out.println("Difference between numbers is equal to: " + resultDifferDoubleNumber);
		
		double resultMultiplyDoubleNumber = number3 * number4;
		System.out.println("Multiplication of numbers is equal to: " + resultMultiplyDoubleNumber);
		
		double resultRemainderDoubleNumber = number3 % number4;
		System.out.println("Remainder of division of numbers is equal to: " + resultRemainderDoubleNumber);
		
		double resultDivideDoubleNumber = number3 / number4;
		System.out.println("Division of numbers is equal to: " + resultDivideDoubleNumber);
	}

}
