package seleniumapi;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.chrome.ChromeDriver;

public class WindowMaximize {
	@Test
	public void testRowSelectionUsingControlKey() throws Exception {
		
		System.setProperty("webdriver.chrome.driver", "E:\\drivers\\chromedriver.exe");

		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();

		driver.get("http://www.google.com");
		
		
		driver.quit();
	}
}
