public class College {

	public static void main(String[] args) {
		Student st1 = new Student("Gosho2", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st2 = new Student("Gosho", "Maths", 4.5 , 2 , 22 , false, 650);
		Student st3 = new Student("Ina", "English", 3.0, 3, 24, false, 0);
		Student st4 = new Student("Maria", "Engineering", 6.0, 4, 25, true, 2000);
		Student st5 = new Student("Ivan", "Physics", 5.3, 3, 28, false, 200);

		StudentGroup gr1 = new StudentGroup("Maths");
		StudentGroup gr2 = new StudentGroup();
		StudentGroup gr3 = new StudentGroup();

		gr1.addStudent(st1);
		gr1.addStudent(st2);
		gr1.addStudent(st3);
		gr1.addStudent(st4);
		gr1.addStudent(st5);

		Student[] stArr = gr1.getStudents();
		for(int i = 0; i < stArr.length; i++)
		{
			System.out.println(String.format("Student %s is group 1.", stArr[i].getName()));
		}
		gr1.addStudent(st2);
		gr1.addStudent(st3);

		
	}

}
