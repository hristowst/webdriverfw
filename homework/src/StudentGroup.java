public class StudentGroup {
	
	String groupSubject;
	Student[] students;
	int freePlaces;
	int studentsCount;
	
	StudentGroup() {
		this.freePlaces = 5;
		this.studentsCount = 0;
		students = new Student[5];
	}

	StudentGroup(String subject) {
		this();
		this.groupSubject = subject;
	}
	
	void addStudent(Student student) {
		if((groupSubject == student.subject) && ((freePlaces - 1) >= 0)) {
			students[studentsCount] = student;
			freePlaces--;
			studentsCount++;
		}
	}
	
	public Student[] getStudents(){
		return this.students;
	}

	public int getStudentCount(){
		return this.studentsCount;
	}
	
	
	
	
}
