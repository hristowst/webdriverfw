import java.util.ArrayList;

public class StudentGroupList {

    String groupSubject;
    ArrayList<Student> students;
    int freePlaces;
    int studentsCount;

    StudentGroupList() {
        this.freePlaces = 5;
        students = new ArrayList<Student>();
    }

    StudentGroupList(String subject) {
        this();
        this.groupSubject = subject;
    }

    void addStudent(Student student) {
        if((groupSubject == student.subject) && (freePlaces >= 1)) {
            students.add(student);
            freePlaces--;
        }
    }

    public ArrayList<Student> getStudents(){
        return this.students;
    }

    public int getStudentCount(){
        return this.studentsCount;
    }




}
